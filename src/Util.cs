using System;

namespace WithLithum.GetLicense;

internal static class Util
{
    internal static int ReturnCode(string error, int code)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.Write($"getlicense#{code}: ");
        Console.ResetColor();
        Console.WriteLine(error);

        return code;
    }
}